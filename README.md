This provides a docker file to run espresso

To build use:

docker build -t registry.gitlab.com/hebbekep/my_espresso .
docker push registry.gitlab.com/hebbekep/my_espresso

Run on meta with singularity, see

https://singularity.lbl.gov/docs-docker
